const Router = require('express');
const router = new Router();
const {check} = require('express-validator');

const controller = require('../controllers/authController');

router.post('/register', [
  check('email', "Email is required").notEmpty(),
  check('email', "Password is required").notEmpty(),
  check('role', "Role is required").notEmpty(),
], controller.registration);

router.post('/login', [
  check('email', "Email is required").notEmpty(),
  check('password', "Password is required").notEmpty(),
], controller.login);

router.post('/forgot_password', [
  check('email', "Email is required").notEmpty(),
], controller.forgotPassword);

module.exports = router;