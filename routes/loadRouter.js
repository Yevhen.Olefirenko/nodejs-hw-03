const Router = require('express');
const router = new Router();
const shipperMiddleware = require('../middleware/shipperMiddleware');
const driverMiddleware = require('../middleware/driverMiddleware');
const authMiddleware = require('../middleware/authMiddleware');
const controller = require('../controllers/loadController');

router.get('/', authMiddleware, controller.getUsersLoads);
router.post('/', shipperMiddleware, controller.addLoad);
router.get('/active', driverMiddleware, controller.getUsersActiveLoad);
router.post('/:id/post', shipperMiddleware, controller.postUserLoadById);

module.exports = router;