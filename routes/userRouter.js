const Router = require('express');
const router = new Router();
const authMiddleware = require('../middleware/authMiddleware');
const controller = require('../controllers/userController');

router.get('/me', authMiddleware, controller.getUserInfo);
router.delete('/me', authMiddleware, controller.deleteUserProfile);
router.patch('/me', authMiddleware, controller.changeUserPassword);

module.exports = router;