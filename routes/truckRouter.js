const Router = require('express');
const router = new Router();
const driverMiddleware = require('../middleware/driverMiddleware');
const controller = require('../controllers/truckController');

router.get('/', driverMiddleware, controller.getUsersTrucks);
router.post('/', driverMiddleware, controller.addTruck);
router.get('/:id', driverMiddleware, controller.getUsersTruckById);
router.put('/:id', driverMiddleware, controller.updateUsersTruckById);
router.delete('/:id', driverMiddleware, controller.deleteUsersTruckById);
router.post('/:id/assign', driverMiddleware, controller.assignTruckToUserById);

module.exports = router;