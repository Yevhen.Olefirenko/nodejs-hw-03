const jwt = require('jsonwebtoken');
const {secret} = require("../config");
const User = require("../models/User");

module.exports = async function(req, res, next) {
  try {
    const token = req.headers.authorization.split(' ')[1];
    if(!token) {
      return res.status(400).json({message: 'User not authorised'});
    }
    const decoded = jwt.verify(token, secret);
    const userId = decoded.id;
    const user = await User.findById(userId);
    if(user.role !== 'SHIPPER') {
      return res.status(400).json({message: 'User doesn\'t have permission'});
    }
    req.user = userId;
    next();
  } catch (e) {
    res.status(400).json({message: 'Bad request'});
  }
};