const Truck = require('../models/Truck');
const bcrypt = require("bcryptjs");
const User = require("../models/User");

class truckController {
  async addTruck(req, res) {
    try {
      const {type} = req.body;
      const userId = req.user;
      const truck = new Truck({type: type, created_by: userId});

      await truck.save();
      return res.status(200).send({message: 'Truck created successfully'});
    } catch (e) {
      console.log(e)
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async getUsersTrucks(req, res) {
    try {
      const userId = req.user;
      const trucks = await Truck.find({created_by: userId}, {
        dimensions: 0,
        payload: 0,
        __v: 0,
      });
      if (!trucks) {
        return res.status(400).json({message: "Bad request"});
      }

      return res.status(200).json({trucks: trucks});
    } catch (e) {
      console.log(e)
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async getUsersTruckById(req, res) {
    try {
      const {id} = req.params;

      if (!id) {
        return res.status(400).json({message: "Bad request"});
      }
      const truck = await Truck.find({_id: id}, {
        dimensions: 0,
        payload: 0,
        __v: 0,
      });

      if (!truck) {
        return res.status(400).json({message: "Bad request"});
      }

      return res.status(200).json({truck: truck});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async updateUsersTruckById(req, res) {
    try {
      const {id} = req.params;
      const {type} = req.body;

      if (!id) {
        return res.status(400).json({message: "Bad request"});
      }
      if (!type) {
        return res.status(400).json({message: "Bad request"});
      }

      const truck = await Truck.findOne({_id: id});

      if (!truck) {
        return res.status(400).json({message: "Bad request"});
      }

      await Truck.updateOne(truck, {type: type});

      return res.status(200).json({message: 'Truck details changed successfully'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async deleteUsersTruckById(req, res) {
    try {
      const {id} = req.params;

      if (!id) {
        return res.status(400).json({message: "Bad request"});
      }

      const truck = await Truck.findOne({_id: id});

      if (!truck) {
        return res.status(400).json({message: "Bad request"});
      }

      await Truck.deleteOne(truck);

      return res.status(200).json({message: 'Truck deleted successfully'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async assignTruckToUserById(req, res) {
    try {
      const {id} = req.params;
      const userId = req.user;

      if (!id) {
        return res.status(400).json({message: "Bad request"});
      }

      const truck = await Truck.findOne({_id: id});

      if (!truck) {
        return res.status(400).json({message: "Truck assigned successfully"});
      }

      await Truck.updateOne(truck, {assigned_to: userId});

      return res.status(200).json({message: 'Truck deleted successfully'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }
}

module.exports = new truckController();
