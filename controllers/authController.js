const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');
const {secret} = require("../config");

const generateAccessToken = (id) => {
  const payload = {id};

  return jwt.sign(payload, secret, {expiresIn: "24h"})
}

class authController {
  async registration(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({message: 'Bad request'});
      }
      const {email, password, role} = req.body;
      const hashPassword = bcrypt.hashSync(password, 7);
      const user = new User({email, password: hashPassword, role});
      await user.save();
      return res.status(200).send({message: 'Profile created successfully'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async login(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({message: 'Bad request'});
      }
      const {email, password} = req.body;
      const user = await User.findOne({email});
      if (!user) {
        return res.status(400).json({message: 'Bad request'});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({message: 'Bad request'});
      }
      const token = generateAccessToken(user._id);
      return res.status(200).send({jwt_token: token});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'})
    }
  }

  async forgotPassword(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({message: 'Bad request'});
      }
      const {email} = req.body;
      const user = await User.findOne({email});
      if (!user) {
        return res.status(400).json({message: 'Bad request'});
      }
      return res.status(200).send({message: 'New password sent to your email address'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'})
    }
  }
}

module.exports = new authController();