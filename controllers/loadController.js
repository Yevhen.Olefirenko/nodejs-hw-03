const Load = require('../models/Load');
const Truck = require('../models/Truck');
const ObjectId = require('mongoose').Types.ObjectId;

const transForLoads = (loads) => {
  return loads.map((load) => {
    load = load.toObject();
    load.logs = load.logs.map((log) => ({
      message: log.message,
      time: log.time,
    }));
    if (!load.assigned_to) {
      load.assigned_to = '';
    }
    if (!load.state) {
      load.state = '';
    }
    return load;
  });
}

const transForLoad = (load) => {
  load = load.toObject();
  load.logs = load.logs.map((log) => ({
    message: log.message,
    time: log.time,
  }));
  if (!load.assigned_to) {
    load.assigned_to = '';
  }
  if (!load.state) {
    load.state = '';
  }
  return load;
};


class loadController {
  async getUsersLoads(req, res) {
    try {
      const userId = req.user.id;
      const isDriver = req.user.role === 'DRIVER';

      let loads;
      if (isDriver) {
        const truckId = await Truck.findOne({assigned_to: userId});

        loads = await Load.find({assigned_to: truckId});
      } else {
        loads = await Load.find({created_by: userId});
      }
      loads = transForLoads(loads);

      res.status(200).json({
        loads: loads,
      });
    } catch (e) {
      console.log(e)
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async addLoad(req, res) {
    try {
      const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
      const userId = req.user;
      const load = new Load({
        created_by: userId,
        name: name,
        payload: payload,
        pickup_address: pickup_address,
        delivery_address: delivery_address,
        dimensions: dimensions
      });

      await load.save();
      return res.status(200).send({message: 'Load created successfully'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async getUsersActiveLoad(request, response) {
    try {

      const userId = request.user;
      const truckId = await Truck.findOne({assigned_to: userId});
      let load = await Load.findOne({
        assigned_to: truckId._id,
        status: 'ASSIGNED',
      }, {__v: 0});

      if (!load) {
        return response.status(400).json({message: 'Bad request'});
      }
      load = transForLoad(load);

      return response.status(200).json({load: load});
    } catch (e) {
      console.log(e)
      response.status(500).json({message: 'Internal server error'});
    }
  }

  async postUserLoadById(req, res) {
    try {
      let driverFound;
      const {id} = req.params;
      const userId = req.user;
      const load = await Load.findOne({_id: id, created_by: userId});

      if (!load) {
        return res.status(400).json({message: 'Bad request'});
      }

      if (load.status !== 'NEW') {
        return res.status(400).json({message: 'Bad request'});
      }

      await Load.updateOne(load, {
            $set: {
              status: 'POSTED',
            },
            $push: {
              logs: {
                message: 'Load status changed to "Posted"',
                time: new Date(),
              },
            }
          }
      );

      const avTruck = await Truck.findOne({
        'status': 'IS',
        'assigned_to': {$ne: null},
        'payload': {$gt: load.payload},
        'dimensions.width': {
          $gt: load.dimensions.width,
        },
        'dimensions.length': {
          $gt: load.dimensions.length,
        },
        'dimensions.height': {
          $gt: load.dimensions.height,
        },
      });

      if (!avTruck) {
        await load.updateOne(
            {
              $set: {
                status: 'NEW',
              },
              $push: {
                logs: {
                  message: `Driver now found, status changed back to "NEW"`,
                  time: new Date(),
                },
              }
            },
        );
        driverFound = false;
      }

      if (avTruck) {
        await load.updateOne(
            {
              $set: {
                status: 'ASSIGNED',
                assigned_to: new ObjectId(avTruck._id),
                state: 'En route to Pick Up',
              },
              $push: {
                logs: {
                  message: `status changed to 'ASSIGNED', driver id: ${avTruck._id}`,
                  time: new Date(),
                },
              }
            },
        );
        driverFound = true;
      }
      avTruck.status = 'OL';
      await avTruck.save();

      return res.status(200).json({
        message: 'Load posted successfully',
        driver_found: driverFound
      });
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }
}

module.exports = new loadController();
