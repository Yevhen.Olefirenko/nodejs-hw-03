const User = require('../models/User');
const bcrypt = require("bcryptjs");

class userController {
  async getUserInfo(req, res) {
    try {
      const id = req.user.id;
      const user = await User.findById(id);
      res.status(200).send({
        "user": {
          "_id": user._id,
          "role": user.role,
          "email": user.email,
          "created_date": user.createdDate,
        }
      });
    } catch (e) {
      res.status(500).json({message: 'Internal server error'})
    }
  }

  async deleteUserProfile(req, res) {
    try {
      const id = req.user.id;
      const result = await User.deleteOne({_id: id});
      if (result.deletedCount === 0) {
        return res.status(400).json({message: 'User not found'});
      }
      res.status(200).send({message: 'Profile deleted successfully'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }

  async changeUserPassword(req, res) {
    try {
      const id = req.user.id;
      const user = await User.findOne({_id: id});
      const {oldPassword, newPassword} = req.body;

      if (!oldPassword || !newPassword) {
        return res.status(400).json({message: 'Bad request'});
      }
      const validPassword = bcrypt.compareSync(oldPassword, user.password);
      if (!validPassword) {
        return res.status(400).json({message: 'Bad request'});
      }
      const hashPassword = bcrypt.hashSync(newPassword, 7);
      await User.updateOne(user, {password: hashPassword});

      res.status(200).send({message: 'Password changed successfully'});
    } catch (e) {
      res.status(500).json({message: 'Internal server error'});
    }
  }
}

module.exports = new userController();
