const {Schema, model} = require('mongoose');

const Truck = new Schema({
  created_by: {
    type: String,
    required: true
  },
  assigned_to: {
    type: String,
    default: null
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  createdDate: {
    type: Date,
    default: Date.now()
  }
});

module.exports = model('Truck', Truck);