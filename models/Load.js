const {Schema, model} = require('mongoose');

const Load = new Schema({
  created_by: {
    type: String,
    required: true
  },
  assigned_to: {
    type: String,
    default: null
  },
  status: {
    type: String,
    enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
    default: 'NEW',
    required: true
  },
  state: {
    type: String,
    enum: ["En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery"],
  },
  name: {
    type: String,
    required: true
  },
  payload: Number,
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },
  logs: {type: [{
      message: {
        type: String,
        default: "Update"
      },
      time: {
        type: Date,
        default: Date.now()
      }
    }]}
  ,
  createdDate: {
    type: Date,
    default: Date.now()
  }
});

module.exports = model('Load', Load);